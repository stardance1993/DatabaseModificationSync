﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DatabaseModificationSync
{
    public class SqlHelper
    {
        public static string DBConnectionString;

        public static SqlSugarClient DBContext
        {
            get
            {
                return new SqlSugarClient(new ConnectionConfig
                {
                    ConnectionString = DBConnectionString,
                    DbType = SqlSugar.DbType.SqlServer,
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.SystemTable
                }) ;
            }
        }
    }
}
