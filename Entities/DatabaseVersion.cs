﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseModificationSync.Entities
{
    public class DatabaseVersion
    {
        private int _iD;


        private string _statement = String.Empty;


        private DateTime _createTime;


        private bool _isExecuted;


        private bool _isSucceed;


        private string _description = String.Empty;


        private string _rESULT = String.Empty;


        private DateTime _executeTime;

        public DatabaseVersion()
        {
            

        }


        public int ID
        {
            get
            {
                return _iD;
            }
            set
            {
                _iD = value;
            }

        }


        public string Statement
        {
            get
            {
                return _statement;
            }
            set
            {
                _statement = value;
            }

        }


        public DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                _createTime = value;
            }

        }


        public bool IsExecuted
        {
            get
            {
                return _isExecuted;
            }
            set
            {
                _isExecuted = value;
            }

        }


        public bool IsSucceed
        {
            get
            {
                return _isSucceed;
            }
            set
            {
                _isSucceed = value;
            }

        }


        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }

        }


        public string RESULT
        {
            get
            {
                return _rESULT;
            }
            set
            {
                _rESULT = value;
            }

        }


        public DateTime ExecuteTime
        {
            get
            {
                return _executeTime;
            }
            set
            {
                _executeTime = value;
            }

        }
    }
}
