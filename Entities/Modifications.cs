﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DatabaseModificationSync.Entities
{
    [XmlRoot(ElementName = "JqSoft")]
    public class Modifications
    {
        [XmlElement(ElementName= "HasExecuted")]
        public bool HasExecuted { get; set; }

        [XmlElement(ElementName = "LastModified")]
        public DateTime LastModified { get; set; }

        [XmlArray(ElementName = "Commands")]
        public List<Command> Commands { get; set; }

    }

    [XmlType(TypeName = "Command")]
    public class Command
    {
        [XmlElement(ElementName ="ID")]
        public int ID { get; set; }
        [XmlElement(ElementName = "Statements")]
        public string Statements { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "CreateTime")]
        public string CreateTime { get; set; }

    }
}
