﻿using DatabaseModificationSync.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DatabaseModificationSync
{
    public class Program
    {
        public static void Main(string[] args)
        {

            string configPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config.ini");
            if (!File.Exists(configPath))
            {
                Console.WriteLine($"找不到配置文件:Config.ini{Environment.NewLine}");
                Console.ReadKey();
            }
            else
            {
                INIHelper inireader = new INIHelper(configPath);
                string server = inireader.ReadValue("Connection", "Server");
                string database = inireader.ReadValue("Connection", "Database");
                string user = inireader.ReadValue("Connection", "User");
                string password = inireader.ReadValue("Connection", "Password");
                string connectionString = $"server={server};database={database};uid={user};pwd={password};";
                new Bootstrapper(connectionString).PrepareSync();
                Console.WriteLine("执行已完成,详情请见日志");
                Console.ReadKey();
            }
        }

        public static void Gene()
        {
            List<DatabaseVersion> dvs = new List<DatabaseVersion>();
            dvs.Add(new DatabaseVersion
            {
                ID = 1,
                CreateTime = DateTime.Now,
                Statement = "select GetDate();"
            });
            dvs.Add(new DatabaseVersion
            {
                ID = 2,
                CreateTime = DateTime.Now,
                Statement = "select GetDate();"
            });
            string ret = JsonConvert.SerializeObject(dvs);
            File.WriteAllText("d:\\demo.json", ret);
        }
    }
}
