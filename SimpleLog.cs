﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DatabaseModificationSync
{
    public class SimpleLog
    {
        public static void Write(string s)
        {
            string LogFullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");
            if (!Directory.Exists(LogFullPath)) Directory.CreateDirectory(LogFullPath);
            File.AppendAllText(Path.Combine(LogFullPath,"DatabaseModificationLog.log"), s);
        }

        public static void Info(string s)
        {
            Write($"{DateTime.Now.ToString("s")}  Info  {s}{Environment.NewLine}");
        }

        public static void Error(string s,Exception ex = null)
        {
            Write($"{DateTime.Now.ToString("s")}  Error  {s}" +
                $"{Environment.NewLine}{ex?.Message}" +
                $"{Environment.NewLine}{ex?.StackTrace}" +
                $"{Environment.NewLine}");
        }
    }
}
